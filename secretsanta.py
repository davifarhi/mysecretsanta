#!/usr/bin/python3
import argparse
from random import randint

#import myconfig as config
import config
from library import Email
from library import check_done, check_ignored


done_list = []
send_list = []
isDone = False

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generate a secret santa list and mail the participants.')
    parser.add_argument('--test', help='Send emails to MAIL_TEST_ADDR.', action="store_true")
    args = parser.parse_args()
    config.TEST = args.test

    if config.TEST:
        list = config.TEST_LIST
    else:
        list = config.LIST
    email = Email(list, test=config.TEST)

    while not isDone:
        done_list = []
        for i in range(len(list)):
            d = randint(0, len(list)-1)

            while (check_done(d, done_list) or check_ignored(i, d, list)
                    or (i != len(list)-1 and d == i)):
                d = randint(0, len(list)-1)

            if i == len(list)-1:
                if i != d:
                    isDone = True
                else:
                    continue

            done_list.append(d)
    
    for i in range(len(list)):
        success = email.Send(i, done_list[i])

        if not success:
            print("There was an error sending the emails")
            break

    email.close()
