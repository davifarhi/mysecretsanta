# Modify the configuration variables below between the commented lines
#==============================================================================
MAIL_SMTP       = "smtp.example.com"
MAIL_SMTP_PORT  = 587
MAIL_ADDR       = "admin@example.com"
MAIL_PSWD       = "PASSWORD"

MAIL_SUBJECT    = "Subject"
MAIL_BODY       = "Hello ${FROM_NAME},\nYou are ${TO_NAME}'s Secret Santa."

LIST = [
    {"name": "Jane", "addr": "jane@example.com", "ignore": []},
    {"name": "Jack", "addr": "jack@example.com", "ignore": []},
    {"name": "Alex", "addr": "alex@example.com", "ignore": []},
    {"name": "Davi", "addr": "davi@example.com", "ignore": []},
    {"name": "Luna", "addr": "luna@example.com", "ignore": []},
]
#==============================================================================
MAIL_TEST_ADDR  = MAIL_ADDR
TEST            = None
TEST_LIST = [
    {"name": "usr1", "addr": MAIL_TEST_ADDR, "ignore": ["usr2", "usr3"]},
    {"name": "usr2", "addr": MAIL_TEST_ADDR, "ignore": []},
    {"name": "usr3", "addr": MAIL_TEST_ADDR, "ignore": []},
    {"name": "usr4", "addr": MAIL_TEST_ADDR, "ignore": ["usr1"]},
]
