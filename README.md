To use this program, modify the variables between the commented lines inside config.py.
If participant "a" should not gift participant "b", add "b" to the ignore list of participant "a" (see example on lines 22 and 25 in config.py).

Launching with the --test option will test the connection and send all emails to MAIL_TEST_ADDR (by default it is equal to MAIL_ADDR)

WARNINGS: 
 - if no result list is possible, the program will loop infinitely, press CTRL+C to exit (no e-mails will be sent)
 - the program does not test for the existance of the e-mail adresses, if one does not exist the process will fail but some e-mails might be sent before the failure
