from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
from datetime import date

#import myconfig as config
import config

# returns true if person "d" was already selected
def check_done(d, list):
    for i in list:
        if d == i:
            return True
    return False

# returns true if person "to" is ignored by the person "from"
def check_ignored(from_id, to_id, list):
    for i in list[from_id]["ignore"]:
        if list[to_id]["name"] == i:
            return True
    return False

class Email:
    def __init__(self, list, test=False):
        self.list = list
        self.test = test
        self.log = ""

        self.server = smtplib.SMTP(config.MAIL_SMTP, config.MAIL_SMTP_PORT)
        self.fromaddr = config.MAIL_ADDR
        self.password = config.MAIL_PSWD

        self.server.ehlo()
        self.server.starttls()
        self.server.ehlo()

        self.server.login(self.fromaddr, self.password)

    def Send(self, from_id, to_id):
        try:
            self.log += ("\n"+self.list[from_id]["name"] +
                         "  --->  "+self.list[to_id]["name"])

            toaddr = self.list[from_id]["addr"]

            msg = MIMEMultipart()
            msg['From'] = self.fromaddr
            msg['To'] = toaddr
            msg['Subject'] = config.MAIL_SUBJECT

            body = config.MAIL_BODY.replace("${FROM_NAME}", self.list[from_id]["name"], 1)
            body = body.replace("${TO_NAME}", self.list[to_id]["name"], 1)

            #body = "Hello "+self.list[from_id]["name"]+"!\n" + \
            #        "Your secret santa is: " + \
            #    self.list[to_id]["name"]+"."

            msg.attach(MIMEText(body, 'plain'))
            text = msg.as_string()

            self.server.sendmail(self.fromaddr, toaddr, text)

            return True

        except Exception as e:
            print(e)
            return False

    def close(self):
        if self.test:
            print(self.log)

        today = date.today()

        msg = MIMEMultipart()
        msg['From'] = self.fromaddr
        msg['To'] = self.fromaddr
        msg['Subject'] = "LOG "+today.strftime("%d/%m/%Y")

        body = self.log

        msg.attach(MIMEText(body, 'plain'))
        text = msg.as_string()
        self.server.sendmail(self.fromaddr, self.fromaddr, text)

        self.server.quit()
